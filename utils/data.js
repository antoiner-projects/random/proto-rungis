const fs = require("fs");

module.exports = {
  writeJson: function(data) {
    fs.writeFile(
      `${__dirname}/../models/data.json`,
      JSON.stringify(data),
      err => {
        if (err) throw err;
      }
    );
  },

  getAllEntries: function() {
    let rawStoredData = fs.readFileSync(`${__dirname}/../models/data.json`);
    return JSON.parse(rawStoredData);
  },

  getEntry: function(key, value) {
    const data = this.getAllEntries();
    return data.find(el => el[key] === value);
  },

  addEntry: function(obj) {
    let data = this.getAllEntries();
    data = data.filter(el => el.id !== obj.id && el.email !== obj.email);
    data.push(obj);
    this.writeJson(data);
  },

  insertPicture: function(userId, picture) {
    let data = this.getAllEntries();
    let updatedObject = null;
    data = data.map(el => {
      if (el.id === userId) {
        this.removePicture(el.picture);
        updatedObject = { ...el, picture };
        return updatedObject;
      }
      return el;
    });
    this.writeJson(data);
    return updatedObject;
  },

  removePicture: function(picture) {
    try {
      if (picture !== "")
        fs.unlinkSync(`${__dirname}/../public/uploads/${picture}`);
    } catch (err) {
      console.log(err);
    }
  }
};
