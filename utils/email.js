const nodeMailer = require("nodemailer");

module.exports = {
  getTransporter: function() {
    return nodeMailer.createTransport({
      host: "smtp.gmail.com",
      port: 465,
      secure: true,
      auth: {
        user: "bilby.soyeux@gmail.com",
        pass: "choucroute22!"
      }
    });
  },

  sendEmail: function(to, url) {
    const transporter = this.getTransporter();
    let mailOptions = {
      to,
      subject: "Proto socket.io",
      html: this.emailTemplate(url)
    };
    transporter.sendMail(mailOptions, (err, info) => {
      if (err) throw err;
    });
  },

  emailTemplate: function(url) {
    return `
      <h1>Proto Socket.io</h1>
      <p>Pour scanner vos documents d'inscription, cliquez ici : </p>
      <a href="${url}">${url}</a>
    `;
  }
};
