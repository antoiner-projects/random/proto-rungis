const express = require("express");
const path = require("path");
const cookieParser = require("cookie-parser");
const bodyParser = require("body-parser");
const logger = require("morgan");
const dotenv = require("dotenv");
const session = require("express-session");
const flash = require("connect-flash");

const routes = require("./routes/index");

dotenv.config();

var app = express();
app.set("views", path.join(__dirname, "views"));
app.set("view engine", "pug");
app.use(express.static(path.join(__dirname, "public")));

app.use(logger("dev"));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cookieParser());

app.use(
  session({
    secret: process.env.SECRET,
    key: process.env.KEY,
    resave: false,
    saveUninitialized: false
  })
);

app.use(flash());

app.locals.env = process.env;
app.use((req, res, next) => {
  res.locals.flashes = req.flash();
  next();
});

app.use("/", routes);

module.exports = app;
