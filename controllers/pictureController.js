const multer = require("multer");
const jimp = require("jimp");
const uuid = require("uuid");

const serverConf = require("./../bin/www");
const emailUtils = require("./../utils/email");
const dataUtils = require("./../utils/data");

const multerOptions = {
  storage: multer.memoryStorage(),
  filtFilter(req, file, next) {
    const isPhoto = file.mimetype.startsWith("image/");
    if (isPhoto) {
      next(null, true);
    } else {
      next({ message: "That filetype isn't allowed" }, false);
    }
  }
};

exports.getHome = (req, res, next) => {
  let user = null;
  if (req.session.user && req.session.user.email) {
    user = req.session.user;
    const entry = dataUtils.getEntry("email", user.email);
    if (user !== entry) {
      req.session.user = entry;
      user = entry;
    }
  }

  res.render("index", { user });
};

exports.getForm = (req, res, next) => {
  res.render("attachment", { userId: req.params.id });
};

exports.getConfirmation = (req, res, next) => {
  res.render("confirmation");
};

exports.getError = (req, res, next) => {
  res.render("error");
};

exports.resetUserSession = (req, res, next) => {
  req.session.user = null;
  res.redirect("/");
};

exports.uploadPicture = multer(multerOptions).single("picture");

exports.resizePicture = async (req, res, next) => {
  if (!req.file) {
    next();
    return;
  }
  const extension = req.file.mimetype.split("/")[1];
  req.body.picture = `${uuid.v4()}.${extension}`;

  const photo = await jimp.read(req.file.buffer);
  await photo.resize(800, jimp.AUTO);
  await photo.write(`./public/uploads/${req.body.picture}`);
  next();
};

exports.addPicture = async (req, res, next) => {
  try {
    const id = parseInt(req.params.id);
    const picture = req.body.picture;

    const updatedObject = dataUtils.insertPicture(id, picture);
    serverConf.io.sockets.emit("newPicture", updatedObject);

    req.flash("success", `Photo ajoutée avec succès !`);
    if (req.headers.referer !== "") return res.redirect(req.headers.referer);
    return res.redirect(`/confirmation`);
  } catch (err) {
    console.log(err);
    req.flash("error", `Erreur lors de l'ajout de la photo :/`);
    if (req.headers.referer !== "") return res.redirect(req.headers.referer);
    return res.redirect(`/error`);
  }
};

exports.connectUserSession = (req, res, next) => {
  try {
    if (!req.body.email) throw "no email in request";

    let entry = dataUtils.getEntry("email", req.body.email);
    if (!entry) {
      const id = Math.floor(10000 + Math.random() * 90000);
      const email = req.body.email;
      entry = { email, id, picture: "" };

      emailUtils.sendEmail(
        email,
        `http://${req.headers.host}/attachment/${id}`
      );
      dataUtils.addEntry(entry);
      req.flash(
        "success",
        `Nouvel utilisateur créé. Vérifiez votre boite de réception !`
      );
    }

    req.session.user = entry;
    return res.redirect(`/`);
  } catch (err) {
    console.log(err);
    req.flash("error", `Echec lors de la connexion`);
    if (req.headers.referer !== "") return res.redirect(req.headers.referer);
    return res.redirect(`/error`);
  }
};

exports.reSendEmail = (req, res, next) => {
  try {
    if (!req.session.user.email || !req.session.user.id) {
      throw "incomplete user in session";
    }
    emailUtils.sendEmail(
      req.session.user.email,
      `http://${req.headers.host}/attachment/${req.session.user.id}`
    );
    req.flash("success", `Mail envoyé, vérifiez votre boite de reception !`);
    return res.redirect(`/`);
  } catch (err) {
    console.log(err);
    req.flash("error", `Echec lors de l'envoi du mail`);
    return res.redirect(`/`);
  }
};
