/*
 * Utils
 */

function debounce(func, wait, immediate) {
  let timeout;
  return function() {
    const context = this,
      args = arguments;
    const later = function() {
      timeout = null;
      if (!immediate) func.apply(context, args);
    };
    let callNow = immediate && !timeout;
    clearTimeout(timeout);
    timeout = setTimeout(later, wait);
    if (callNow) func.apply(context, args);
  };
}

function validateEmail(mail) {
  if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(mail)) return true;
  return false;
}

/*
 * Core JS
 */

//index page

const emailInput = document.getElementById("email-input");
const emailSubmit = document.getElementById("email-submit");

if (emailInput) {
  emailInput.addEventListener(
    "keyup",
    debounce(e => {
      if (validateEmail(e.target.value)) {
        emailSubmit.disabled = false;
      } else {
        emailSubmit.disabled = true;
      }
    }, 500)
  );
}

const userEmailEl = document.querySelector("[data-user-email]");
const socket = io();
socket.on("newPicture", data => {
  if (
    userEmailEl &&
    userEmailEl.getAttribute("data-user-email") === data.email
  ) {
    const img = document.getElementById("target-img");
    img.src = `/uploads/${data.picture}`;
  }
});

//attachment page
const imageInput = document.getElementById("picture-input");
const label = document.querySelector("#picture-input + label");
if (imageInput) {
  const labelText = label.textContent;
  imageInput.addEventListener("change", e => {
    const fieldSet = document.querySelector("form.attachment-form fieldset");
    if (e.target.files[0]) {
      const pictureEl = document.getElementById("preview-picture");
      pictureEl.src = URL.createObjectURL(e.target.files[0]);
      fieldSet.disabled = false;
      label.textContent = "Reprendre une photo";
    } else {
      fieldSet.disabled = true;
      label.textContent = labelText;
    }
  });
}
