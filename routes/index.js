var express = require("express");
var router = express.Router();
const pictureController = require("./../controllers/pictureController");

/* GET home page. */
router.get("/", pictureController.getHome);

/* GET attachment page (from mobile) */
router.get("/attachment/:id", pictureController.getForm);

/* GET confirmation page */
router.get("/confirmation", pictureController.getConfirmation);

/* GET error page*/
router.get("/error", pictureController.getError);

/* POST attachment form data */
router.post(
  "/add/:id",
  pictureController.uploadPicture,
  pictureController.resizePicture,
  pictureController.addPicture
);

/* POST connect user form */
router.post("/connect-user", pictureController.connectUserSession);

/* POST reset user session */
router.post("/reset-session", pictureController.resetUserSession);

/* POST send email form */
router.post("/send-email", pictureController.reSendEmail);

module.exports = router;
